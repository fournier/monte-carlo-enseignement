noweave -delay -index -latex ccr_V0_V1.nw > ccr_V0_V1.tex
pdflatex ccr_V0_V1.tex
notangle -Rcompilation.bash ccr_V0_V1.nw > compilation.bash
notangle -Rmain.c ccr_V0_V1.nw > main.c
gcc -c main.c -I /home/rfo/Transfert-local/Cora/EDSTAR/Star-Engine-0.6.0-GNU-Linux64/include

gcc -o view_factor main.o -L /home/rfo/Transfert-local/Cora/EDSTAR/Star-Engine-0.6.0-GNU-Linux64/lib -ls3d -ls3daw -lssp -lm -Wl,-rpath=/home/rfo/Transfert-local/Cora/EDSTAR/Star-Engine-0.6.0-GNU-Linux64/lib
