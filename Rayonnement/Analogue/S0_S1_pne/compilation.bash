noweave -delay -index -latex analogue_S0_S1.nw > analogue_S0_S1.tex
pdflatex analogue_S0_S1.tex
notangle -Rcompilation.bash analogue_S0_S1.nw > compilation.bash
notangle -Rmain.c analogue_S0_S1.nw > main.c
gcc -c main.c -I /home/rfo/Transfert-local/Cora/EDSTAR/Star-Engine-0.6.0-GNU-Linux64/include

gcc -o view_factor main.o -L /home/rfo/Transfert-local/Cora/EDSTAR/Star-Engine-0.6.0-GNU-Linux64/lib -ls3d -ls3daw -lssp -lm -Wl,-rpath=/home/rfo/Transfert-local/Cora/EDSTAR/Star-Engine-0.6.0-GNU-Linux64/lib
